import pickle
from pathlib import Path
from read_data import read_json, read_metadata, extractor_seq, get_info_metadata
from typing import List, Dict
from clases import Pair, Position, TimeInfo, DataItem, GNSSDataset
from datetime import datetime, timedelta
import time
from pydantic.dataclasses import dataclass
from rich import print
from tslearn.clustering import TimeSeriesKMeans
from tslearn.clustering import silhouette_score
import numpy as np
import ujson as json

"""
Leer directorio
Cada estación con su 'i'

"""


def read_json_section(
        path,
        sections,
        index):

    if path.is_dir():
        megadataset = {}
        for i, file_path in enumerate(path.rglob("*")):

            station = file_path.parent.name
            name = file_path.stem
            try:
                if file_path.exists() and name == f"{index}":
                    datab = file_path.read_bytes()
                    data = pickle.loads(datab)
                    megadataset[station] = data
            except Exception as ex:
                print("Error", path, ex)
        return megadataset
    return []


if __name__ == "__main__":
    hours_lapso = 12
    base_path = Path("./datasets")
    path_sections = Path("data_sections.data")
    databytes = path_sections.read_bytes()
    sections = pickle.loads(databytes)

    print(sections)
    tramos = max([len(v) for v in sections.values()])
    # hacer el análisis para cada serie de días
    print(f"Tramos {tramos}")
    for i in range(tramos):
        # crear las pendientes
        path_pdts = Path(f"pendientes_{i}.data")
        if not path_pdts.exists():
            megadataset = read_json_section(base_path, sections, i)
            gnss_dataset = GNSSDataset(megadataset)
            tmin, tmax = gnss_dataset.abs_min_max
            st = time.process_time()
            pdtes = gnss_dataset.pendientes()
            et = time.process_time()
            res = et - st
            print('CPU Execution time:', res, 'seconds')
            databytes = pickle.dumps(pdtes)
            path_pdts.write_bytes(databytes)
        else:
            print("Create pendientes ya")
            path = Path("../DATA_BACKUP")
            metadata = read_metadata(path)
            databytes = path_pdts.read_bytes()
            pdtes = pickle.loads(databytes)
            secuencias = extractor_seq(pdtes)
            get_info_metadata(secuencias, metadata)
            path_y = Path(f"y_results_{i}.data")
            model_info = {"secuencias": secuencias}
            seqname = f"secuencia_{i}"

            X_arrays = {}

            for station, seqs in secuencias.items():
                for k, vals in seqs.items():
                    if type(k) == int:
                        # se agrupan por columna
                        key = f"{k}"
                        if key not in X_arrays:
                            X_arrays[key] = {
                                "E": [],
                                "N": [],
                                "U": []}
                        # verificar al menos tenga un valor no nan (None)
                        X_arrays[key]["E"].append(vals["E"])
                        X_arrays[key]["U"].append(vals["U"])
                        X_arrays[key]["N"].append(vals["N"])

            model_info["X_arrays"] = X_arrays
            st = time.process_time()

            experimento = {}
            for nc in range(7, 12):
                fields = ["E", "N", "U"]
                tskm = {}

                for f in fields:
                    tskm[f] = TimeSeriesKMeans(
                        n_clusters=nc,
                        n_init=2,
                        metric="dtw", verbose=False,
                        max_iter_barycenter=10,
                        random_state=0)

                for key, item in X_arrays.items():
                    for field in fields:
                        # se agrupan por columna-campo-nro-exp
                        new_key = f"{key}_{field}_{nc}"
                        x_array = np.array(item[field])
                        results = tskm[field].fit(x_array)

                # nos daria 3x7 graficas _______
                experimento[nc] = {eje: tskm[eje].inertia_ for eje in fields}
            model_info["experimento"] = experimento

            path = Path(f"secuencias/model_info_{seqname}.data")
            databytes = pickle.dumps(model_info)
            path.write_bytes(databytes)
