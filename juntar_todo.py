from pathlib import Path
from collections import OrderedDict
import numpy as np
import pandas as pd
import pickle
import umap
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans


def read_path(path):
    databytes = path.read_bytes()
    objeto = pickle.loads(databytes)
    return objeto


if __name__ == "__main__":
    y_path = Path("y_results.data")

    for sec_path in Path("./secuencias").rglob("*.data"):
        #     sec_path = Path(f"model_info_secuencia.data")
        #     Y_results = read_path(y_path)
        model_info = read_path(sec_path)
        secuencias = model_info["secuencias"]

        dataset = []
        basic_fields = ["lat", "lon", "z"]
        fields = ["N", "E", "U"]
        estaciones = []
        positions = []
        for pos, (station, seqs) in enumerate(secuencias.items()):
            estaciones.append(pos)
            lat = seqs["lat"]
            lon = seqs["lon"]
            llpos = [lon, lat]
            positions.append(llpos)
            # columna k, secuencia: vals = {"N","E","U"}
            # extraer info de UNA estacion en base a su metadata y series
            # tiempo en ventanas de secuencia
            item = {b: seqs[b] for b in basic_fields}
            for k, vals in seqs.items():
                if k not in basic_fields:
                    for f in fields:
                        key_name = f"{k}_{f}"
                        y_array = Y_results[key_name]
                        y_val = y_array[pos]
                        item[key_name] = y_val
                        x_val = vals[f]
                        v_std = np.array(x_val).std()
                        v_mean = np.array(x_val).mean()
                        v_median = np.median(np.array(x_val))
                        item[f"{key_name}_std"] = v_std
                        item[f"{key_name}_mean"] = v_mean
                        item[f"{key_name}_median"] = v_median
                        # esto es item: un diccionario que repr info por
                        # estacion y secuencias por columna
            dataset.append(item)
        df = pd.DataFrame(dataset)
        reducer = umap.UMAP()
        embedding = reducer.fit_transform(df)

        cmap = 'rainbow'

        plt.scatter(
            embedding[:, 0],
            embedding[:, 1],
            cmap=cmap)
        plt.gca().set_aspect('equal', 'datalim')
        plt.title('UMAP projection of the Stations dataset', fontsize=18)
        plt.legend()
        plt.show()

        print(df.head())

        print(df.head())
        print(df.shape)

    # # Generar datos de ejemplo
    # np.random.seed(3)
    # n_samples = 200
    # X = embedding

    # # Definir el número de clusters (K)
    # n_clusters = 4

    # # Crear y entrenar el modelo K-means
    # kmeans = KMeans(n_clusters=n_clusters)
    # kmeans.fit(X)

    # # Obtener las etiquetas de los clusters y los centroides
    # labels = kmeans.labels_
    # centroids = kmeans.cluster_centers_

    # # Visualizar los resultados
    # plt.scatter(X[:, 0], X[:, 1], c=labels)
    # plt.scatter(centroids[:, 0], centroids[:, 1], marker='x', color='red')
    # plt.xlabel('Dimensión 1')
    # plt.ylabel('Dimensión 2')
    # plt.title('K-means Clustering based on UMAP embeddings')
    # plt.show()
    # df2 = pd.DataFrame(positions)
    # plt.scatter(
    #     df2.iloc[:, 0],
    #     df2.iloc[:, 1], c=labels)

    # plt.gca().set_aspect('equal', 'datalim')
    # plt.title('Map of stations in Chile', fontsize=18)
    # plt.legend()
    # plt.show()

    # print(df.head())
    # print(df.shape)
