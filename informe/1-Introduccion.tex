\section{Introducción}
% contexto general del problema en estudio

% El dataset

Aceleración, velocidad y desplazamiento son los tres tipos de observaciones utilizados en
sismología en la actualidad. En este sentido, el Centro Sismológico Nacional (CSN) de la
Universidad de Chile, opera y mantiene la Red Sismológica Nacional (RSN) la que observa
estos parámetros en tiempo real, y son utilizados en forma independiente y conjunta,
mediante diferentes modelos, para estimar el hipocentro y magnitud de eventos sísmicos.

Como los diferentes sensores utilizados sufren igualmente de saturación de las señales, en
el caso de eventos de magnitud superior a Mw7.0, para aquellos sensores de velocidad y
aceleración que están cercanos al epicentro, el desplazamiento se torna importante, ya que
estos sensores no tienen problemas de saturación (Leyton et al., 2018)\cite{b1}.

El desplazamiento es derivado de la posición estimada con receptores GNSS, con
observaciones a 1 Hz en tiempo real. Este valor presenta la mejor solución que
puede ser estimada en la actualidad en tiempo real, con un error entre 5 a 7 centímetros,
medidos sobre el ruido de la propia observación. 

% El problema

El desafío es poder caracterizar este ruido, dado que es generado por efectos no corregidos en la observación, los cuales se presentan por igual en las estaciones. Este ruido puede ser tanto por errores de medición, efectos atmosféricos y de cálculo de la posición, entre otros. El ruido que se presenta en las estaciones puede ser identificado mediante la detección de patrones en sus señales, que se manifiestan al considerar las curvas de las serie tiempo para sus tres ejes principales, componentes este-oeste, norte-sur y arriba-abajo. 

A simple vista, el análisis de las curvas permite observar que hay comportamientos similares en estaciones cercanas, presentando un ruido o una curva característica muy parecida, lo que permite plantear la hipótesis de que sea posible reducir ese ruido, considerando que ese grupo no está observando algún fenómeno sísmico significativo. Una forma de encontrar y analizar los patrones emergentes de este conjunto de datos, sería una técnica de reducción de dimensionalidad, entre las que destacan UMAP, t-SNE y PaCMAP (Wang Y., et.al., 2021)\cite{b3}.

UMAP, del inglés “Uniform Manifold Approximation and Projection”, es una técnica de reducción de dimensionalidad que mapea datos de alta dimensión a un espacio de baja dimensión preservando la estructura y relaciones entre los datos. Para esto, genera una representación de los datos en un espacio topológico con el fin de capturar estructuras globales y locales aprovechando las propiedades de continuidad específicas de este espacio (L. McInnes, et.al., 2020)\cite{b2}. Es utilizado en varias aplicaciones: Visualización de datos de alta dimensionalidad en 2 o 3 dimensiones, clusterización y detección de anomalías, genómica y transcriptómica a resolución de una sola célula (scRNAseq) e incluso en procesamiento de lenguaje natural.

UMAP basa su algoritmo en una aproximación de la topología de los datos de cada punto, representado en un grafo, es un 0-símplice conectado a otros puntos, formando 1-símplices o 2-símplices. Para esto se siguen los siguientes pasos: (1) Construcción de un grafo con k vecinos cercanos ponderados en base a una métrica donde la más utilizada es la distancia euclideana, i.e., el peso de las aristas está determinada por la distancia entre puntos y los puntos pertenecen al mismo subset cuando sus radios se sobreponen. (2) Ampliación o reducción de radios dependiendo de la densidad de puntos en el grafo en alta dimensión basado en la distancia al k-ésimo vecino más cercano (uno de los hiper-parámetros del algoritmo). (3) Proyección del gráfico obtenido en alta dimensión a baja dimensión en base a una minimización de la entropía cruzada de los sets simpliciales, el algoritmo Stochastic Gradient Descent (SGD) y aplicando una fuerza repulsiva entre puntos para mantener las distancias entre ellos (L. McInnes, et.al., 2020 y Parcalabescu L., 2021)\cite{b2, b4}.

UMAP se distingue de su competidor más cercano, t-SNE, que utiliza una aproximación similar, en varios aspectos, entre ellos la escalabilidad, ya que su algoritmo le permite analizar conjuntos de datos de millones de puntos de forma eficiente. Otro aspecto en el que destaca UMAP por sobre t-SNE es la preservación de la estructura global y local, capturando mejor las relaciones entre datos a diferentes escalas. Finalmente, UMAP es más resiliente a cambios en los hiper-parámetros, obteniendo resultados menos variables ante su modificación (Wang Y., et.al., 2021)\cite{b3}.

% Métodos a evaluar

El enfoque de este trabajo consistirá entonces en analizar \textit{streams} de datos de tipo GNSS, sobre los que se ha realizado un corte de un día para un análisis fijo del tramo. Este conjunto puede ser representado de manera ordenada según su ubicación geográfica y agrupado tanto por su forma de curva específica, así como también de manera general mediante el uso de la técnica de \textit{umap} que permite entregar una representación 2D de toda la información que se le provee al modelo.

% objetivo

El objetivo principal de este trabajo es detectar patrones según la forma de la señal y la ubicación geográfica del sensor que la genera.

% objetivos específicos

Entre los objetivos específicos se encuentran el análisis y procesamiento de la señal, minería de datos sobre las señales, creación de una estrategia o metodología que represente cada serie de datos, aplicar clusterización solo por similitud de curvas, creación de un dataset representativo, aplicar UMAP frente al dataset y determinar los grupos finales.
