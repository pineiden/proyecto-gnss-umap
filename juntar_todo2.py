from pathlib import Path
from collections import OrderedDict
import numpy as np
import pandas as pd
import pickle 
import umap
import matplotlib.pyplot as plt
import seaborn as sns

def read_path(path):
    databytes = path.read_bytes()
    objeto = pickle.loads(databytes)
    return objeto


if __name__ == "__main__":
    y_path = Path("y_results.data")
    sec_path = Path(f"model_info_secuencia.data")

    Y_results = read_path(y_path)
    model_info = read_path(sec_path)

    secuencias = model_info["secuencias"]

    dataset = []
    positions = []
    basic_fields = ["lat", "lon", "z"]
    fields = ["N", "E", "U"]
    selected_curve = 192
    selected_field = "U"
    for pos, (station, seqs) in enumerate(secuencias.items()):
        # columna k, secuencia: vals = {"N","E","U"}
        # extraer info de UNA estacion en base a su metadata y series
        # tiempo en ventanas de secuencia
        X = seqs[selected_curve][selected_field]
        v_media = np.array(X).mean()
        v_std = np.array(X).std()
        X.append(v_media)
        X.append(v_std)
        dataset.append(X)
        lat = seqs["lat"]
        lon = seqs["lon"]
        pos = [lon,lat]
        positions.append(pos)

    y = Y_results[f'{selected_curve}_{selected_field}']
    df=pd.DataFrame(dataset)
    reducer = umap.UMAP()
    embedding = reducer.fit_transform(df)
    print(embedding.shape)
    plt.scatter(
        embedding[:, 0],
        embedding[:, 1], c=y, cmap='rainbow')
    plt.gca().set_aspect('equal', 'datalim')
    plt.title(f'UMAP projection for curve {selected_curve} in field {selected_field}', fontsize=20)
    plt.legend()
    plt.show()
    df2 = pd.DataFrame(positions)
    print(df2.shape)
    plt.scatter(
        df2.iloc[:, 0],
        df2.iloc[:, 1], c=y, cmap='rainbow')
    plt.gca().set_aspect('equal', 'datalim')
    plt.title(f'Map of stations for curve {selected_curve} in field {selected_field}', fontsize=20)
    plt.legend()
    plt.show()
    
    print(df.head())
    print(df.shape)
