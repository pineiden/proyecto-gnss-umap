import numpy as np
from sklearn.datasets import load_digits
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from rich import print
from pathlib import Path
import requests
import io
import umap
from matplotlib import pyplot

if __name__ == "__main__":
    url = "https://raw.githubusercontent.com/allisonhorst/palmerpenguins/c19a904462482430170bfe2c718775ddb7dbb885/inst/extdata/penguins.csv"
    csv_path = Path(__file__).parent / "penguins.csv"
    if csv_path.exists():
        print("existe")
        penguins = pd.read_csv(csv_path)
    else:
        print("se descarga")
        s = requests.get(url).content
        csv_path.write_text(s.decode('utf-8'))
        penguins = pd.read_csv(csv_path)

    print(penguins.head())
    penguins = penguins.dropna()
    print(penguins.species.value_counts())
    sns.pairplot(penguins.drop("year", axis=1), hue='species')
    pyplot.show()
    reducer = umap.UMAP()
    penguin_data = penguins[
        [
            "bill_length_mm",
            "bill_depth_mm",
            "flipper_length_mm",
            "body_mass_g",
        ]
    ].values
    scaled_penguin_data = StandardScaler().fit_transform(penguin_data)
    embedding = reducer.fit_transform(scaled_penguin_data)
    print(embedding.shape)
    plt.scatter(
        embedding[:, 0],
        embedding[:, 1],
        c=[sns.color_palette()[x] for x in penguins.species.map({"Adelie": 0, "Chinstrap": 1, "Gentoo": 2})])
    plt.gca().set_aspect('equal', 'datalim')
    plt.title('UMAP projection of the Penguin dataset', fontsize=24)
    pyplot.show()
