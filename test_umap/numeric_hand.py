import numpy as np
from sklearn.datasets import load_digits
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from rich import print
from pathlib import Path
import requests
import io
from matplotlib import pyplot
import umap.umap_ as umap

digits = load_digits()
print(digits.DESCR)

fig, ax_array = plt.subplots(20, 20)
axes = ax_array.flatten()
for i, ax in enumerate(axes):
    ax.imshow(digits.images[i], cmap='gray_r')
plt.setp(axes, xticks=[], yticks=[], frame_on=False)
plt.tight_layout(h_pad=0.5, w_pad=0.01)
pyplot.show()

digits_df = pd.DataFrame(digits.data[:, 1:11])
digits_df['digit'] = pd.Series(digits.target).map(
    lambda x: 'Digit {}'.format(x))
sns.pairplot(digits_df, hue='digit', palette='Spectral')

pyplot.show()

reducer = umap.UMAP(a=None, angular_rp_forest=False, b=None,
                    force_approximation_algorithm=False, init='spectral', learning_rate=1.0,
                    local_connectivity=1.0, low_memory=False, metric='euclidean',
                    metric_kwds=None, min_dist=0.1, n_components=2, n_epochs=None,
                    n_neighbors=15, negative_sample_rate=5, output_metric='euclidean',
                    output_metric_kwds=None, random_state=42, repulsion_strength=1.0,
                    set_op_mix_ratio=1.0, spread=1.0, target_metric='categorical',
                    target_metric_kwds=None, target_n_neighbors=-1, target_weight=0.5,
                    transform_queue_size=4.0, transform_seed=42, unique=False, verbose=False)

embedding = reducer.fit_transform(digits.data)
# Verify that the result of calling transform is
# idenitical to accessing the embedding_ attribute
assert(np.all(embedding == reducer.embedding_))

print(reducer.fit(digits.data))

plt.scatter(
    embedding[:, 0],
    embedding[:, 1],
    c=digits.target,
    cmap='Spectral',
    s=5)

plt.gca().set_aspect('equal', 'datalim')
plt.colorbar(boundaries=np.arange(11)-0.5).set_ticks(np.arange(10))
plt.title('UMAP projection of the Digits dataset', fontsize=24)
pyplot.show()
