from tslearn.clustering import TimeSeriesKMeans
import pickle
from pathlib import Path
import matplotlib.pyplot as plt
import numpy as np
import time

plt.rcParams['figure.figsize'] = [25, 8]


def grafica(eje, curva):
    x, y = zip(*curva)
    plt.plot(x, y, 'bx-')
    plt.xlabel('k')
    plt.ylabel('Sum_of_squared_distances')
    plt.title(f' {eje} - Elbow Method For Optimal k')
    plt.show()


if __name__ == "__main__":
    for sec_path in Path("./secuencias").rglob("*.data"):
        #     sec_path = Path(f"model_info_secuencia.data")
        #     Y_results = read_path(y_path)

        databytes = sec_path.read_bytes()
        model_info = pickle.loads(databytes)
        print(sec_path)

    # print("Model Info loaded")
    # # evaluar visualmente lo que resulta de modelar con metodo del
    # # codo
    # curvas = {"E": [], "N": [], "U": []}
    # for k, v in model_info["experimento"].items():
    #     print(k)
    #     for name, vals in v.items():
    #         print(name, vals)
    #         curvas[name].append((k, vals))

    # print(curvas)
    # """
    # E: 10
    # U: 8
    # N: 9
    # """
    # X_arrays = model_info["X_arrays"]
    # Y_results = {}
    # grafica("E", curvas["E"])
    # grafica("N", curvas["N"])
    # grafica("U", curvas["U"])

    # # cantidad de clusters por eje
    # fields = {"E": 10, "N": 9, "U": 8}
    # tskm = {}
    # path = Path("modelos_ts_cluster.data")

    # if not path.exists():
    #     for f, nc in fields.items():
    #         tskm[f] = TimeSeriesKMeans(
    #             n_clusters=nc,
    #             n_init=2,
    #             metric="dtw", verbose=False,
    #             max_iter_barycenter=10,
    #             random_state=0)

    #     st = time.process_time()

    #     for key, item in X_arrays.items():
    #         for field in fields:
    #             # se agrupan por columna-campo-nro-exp
    #             new_key = f"{key}_{field}"
    #             print(new_key)
    #             x_array = np.array(item[field])
    #             results = tskm[field].fit(x_array)

    #     databytes = pickle.dumps(tskm)
    #     path.write_bytes(databytes)

    #     et = time.process_time()
    #     res = et - st

    # else:
    #     databytes = path.read_bytes()
    #     tskm = pickle.loads(databytes)
    # print("====")

    # for column, X_array in X_arrays.items():
    #     for f in fields:
    #         model = tskm[f]
    #         y_pred = model.predict(X_array[f])
    #         Y_results[f"{column}_{f}"] = y_pred

    # print("Yresults", Y_results.keys())
    # for k, val in Y_results.items():
    #     print(k, len(val))
    # path = Path("y_results.data")
    # databytes = pickle.dumps(Y_results)
    # path.write_bytes(databytes)
    # print("Model Info saved", path)
    # # evaluar visualmente lo que resulta de modelar con metodo del codo
