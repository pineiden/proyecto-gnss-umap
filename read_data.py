import numpy as np
from tslearn.clustering import TimeSeriesKMeans
from tslearn.clustering import silhouette_score
import pickle
import time
import ujson as json
from pathlib import Path
from datetime import datetime

from typing import List, Dict
from dacite import from_dict
from pydantic.dataclasses import dataclass
from pydantic import BaseModel, validator
from rich import print
from collections import OrderedDict
from clases import Pair, Position, TimeInfo, DataItem, GNSSDataset


def extractor_seq(
        pendientes,
        # tstart: int,  # secs
        # tend: int,  # secs
        ventana_secs: int = 30,
        delta: int = 5):
    # espacios para recuperar
    ventana = int(ventana_secs//delta)
    series_seq = {}
    # determinar max columns
    # estacion con mayor cantidad de tiempo

    st_min = min(pendientes, key=lambda x: pendientes[x][0]["ts"])
    st_max = max(pendientes, key=lambda x: pendientes[x][0]["ts"])

    ts_min = pendientes[st_min][0]["ts"]
    ts_max = pendientes[st_max][-1]["ts"]

    max_len_pdt = max(pendientes, key=lambda x: len(pendientes[x]))
    # print("stmin", st_min,  pendientes[st_min][0], ts_min)
    # print("stmax", st_max, pendientes[st_max][-1], ts_max)

    W = len(pendientes[max_len_pdt])
    print("max pdts", len(pendientes[max_len_pdt]))
    print("max pdts tmin", pendientes[max_len_pdt][0]["ts"], ts_min)
    print("max pdts tmax", pendientes[max_len_pdt][-1]["ts"], ts_max)
    # breakpoint()
    diferencia = ((ts_max - ts_min)//delta)
    #
    for station, dataset_pdt in pendientes.items():
        n = len(dataset_pdt)
        serie = OrderedDict()
        columna = 0
        ts_min = pendientes[st_min][0]["ts"]
        for i in range(0, n-ventana+1, ventana):
            seq = dataset_pdt[i:i+ventana]
            _seq = {
                "ts": [s.get("ts") for s in seq],
                "N": [s.get("N", 0) for s in seq],
                "E": [s.get("E", 0) for s in seq],
                "U": [s.get("U", 0) for s in seq],
            }
            serie[columna] = _seq
            columna += 1
            # verificar que la seq este entre
            # ts_min y ts_min+ventana_secs
            # si es asi, asignar columna
            # si se sale del borde bajar el rescate del ultimo valor y compara
            # si no, aumentar columna
            # y desplazar la ventana a
            # ts_min" = ts_min + ventana_secs
            # hasta ts_min" + ventana_secs
        series_seq[station] = serie
    return series_seq


def read_json(path: Path) -> List[DataItem]:
    if path.exists():
        print(f"Reading {path}")
        datatxt = path.read_text()
        try:
            dataset = json.loads(datatxt)
            return [from_dict(data_class=DataItem, data=d) for d in
                    dataset]
        except Exception as ex:
            print("Error", path, ex)
    return []


def read_dir_jsons(path: Path) -> Dict[str, List[DataItem]]:
    megadataset = {}
    if path.is_dir():
        for i, file_path in enumerate(path.rglob("*.json")):
            print("new json", i, "->", file_path)
            name = file_path.stem
            try:
                if not name == "metadata":
                    megadataset[name] = read_json(file_path)
            except Exception as ex:
                print("Error", path, ex)
        return megadataset
    return []


def read_metadata(path: Path) -> Dict[str, List[DataItem]]:
    if path.is_dir():
        metadata_file = path / "metadata.json"
        datatxt = metadata_file.read_text()
        dataset = json.loads(datatxt)
        return {d["code"]: d for d in dataset}
    return []


def get_info_metadata(secuencias, metadata):
    borrar = []
    for k, v in secuencias.items():
        if k in metadata:
            v.update(metadata[k]["position"]["llh"])
        else:
            borrar.append(k)

    for k in borrar:
        del secuencias[k]


if __name__ == "__main__":
    path_pdts = Path("pendientes.data")
    if not path_pdts.exists():
        path = Path("../DATA_BACKUP/SBLL.json")
        dataset = read_json(path)

        # for data in dataset:
        #     print(data.usar)

        path = Path("../DATA_BACKUP")
        dataset = read_dir_jsons(path)
        print("Build GNSS Dataset")
        gnss_dataset = GNSSDataset(dataset)
        # print(gnss_dataset)
        # print(gnss_dataset.min_max)
        tmin, tmax = gnss_dataset.abs_min_max
        print("Seconds", tmax-tmin)
        print(gnss_dataset.abs_min_max)
        print("Bloque==>")
        bloque = gnss_dataset.seleccion(["PAZU", "RMBA", "SAAV", "PCCL"])
        print(bloque)
        print("Pendientes calculo")
        print(bloque.pendientes().keys())

        print("Pendientes calculo")
        st = time.process_time()

        pdtes = gnss_dataset.pendientes()
        print(pdtes)
        et = time.process_time()
        res = et - st
        print('CPU Execution time:', res, 'seconds')
        databytes = pickle.dumps(pdtes)
        path_pdts.write_bytes(databytes)
    else:
        path = Path("../DATA_BACKUP")
        metadata = read_metadata(path)
        databytes = path_pdts.read_bytes()
        pdtes = pickle.loads(databytes)
        secuencias = extractor_seq(pdtes)
        print("secuencias ok")
        get_info_metadata(secuencias, metadata)

        # for st, seqs in secuencias.items():
        #     for k, vals in seqs.items():
        #         if type(k) != int:
        #             print(k, vals)
        #         else:
        #             print(k, len(vals))
        #         print(seqs)
        # por cada COLUMNA de secuencia
        # hacer clustering sobre la serie tiempo
        # hay que probar la mejor cantidad de cluster por grupo

        path_y = Path("y_results.data")

        model_info = {"secuencias": secuencias}
        seqname = "secuencia"
        if False:
            seqname = ""
            X_arrays = {}

            for station, seqs in secuencias.items():
                for k, vals in seqs.items():
                    if type(k) == int:
                        # se agrupan por columna
                        key = f"{k}"
                        if key not in X_arrays:
                            X_arrays[key] = {
                                "E": [],
                                "N": [],
                                "U": []}
                        # verificar al menos tenga un valor no nan (None)
                        X_arrays[key]["E"].append(vals["E"])
                        X_arrays[key]["U"].append(vals["U"])
                        X_arrays[key]["N"].append(vals["N"])

            model_info["X_arrays"] = X_arrays
            st = time.process_time()

            experimento = {}
            for nc in range(7, 12):
                fields = ["E", "N", "U"]
                tskm = {}

                for f in fields:
                    tskm[f] = TimeSeriesKMeans(
                        n_clusters=nc,
                        n_init=2,
                        metric="dtw", verbose=False,
                        max_iter_barycenter=10,
                        random_state=0)

                for key, item in X_arrays.items():
                    for field in fields:
                        # se agrupan por columna-campo-nro-exp
                        new_key = f"{key}_{field}_{nc}"
                        print(new_key)

                        x_array = np.array(item[field])
                        results = tskm[field].fit(x_array)

                # nos daria 3x7 graficas _______
                experimento[nc] = {eje: tskm[eje].inertia_ for eje in fields}
            model_info["experimento"] = experimento

            et = time.process_time()
            res = et - st
            print("Model Info saved", path, "elapsed", res)
        else:
            path = Path(f"model_info_{seqname}.data")
            databytes = pickle.dumps(model_info)
            path.write_bytes(databytes)
        # evaluar visualmente lo que resulta de modelar con metodo del codo
