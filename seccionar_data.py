import pickle
from pathlib import Path
from read_data import read_json
from typing import List, Dict
from clases import Pair, Position, TimeInfo, DataItem, GNSSDataset
from datetime import datetime, timedelta
import time
from pydantic.dataclasses import dataclass
from rich import print


def get_min_max_dt(path: Path):
    if path.is_dir():
        primeros = []
        ultimos = []
        last_index = {}
        megadataset = {}
        for i, file_path in enumerate(path.rglob("*.json")):
            name = file_path.stem
            try:
                if not name == "metadata":
                    print("get min max ... i:>", i)
                    data = read_json(file_path)
                    data.sort(key=lambda x: x.timestamp)
                    first = datetime.fromisoformat(data[0].dt_gen)
                    last = datetime.fromisoformat(data[-1].dt_gen)
                    primeros.append(first)
                    ultimos.append(last)
                    last_index[name] = 0
                    megadataset[name] = []
                    del data
            except Exception as ex:
                print("Error", path, ex)
        return (min(primeros), max(ultimos), last_index, megadataset)
    return []


# @dataclass
# class Slice:
#     start: int
#     end: int

#     def slice(self):
#         return slice(self.start, self.end)


def read_json_section(
        path,
        start_date,
        end_date,
        last_index,
        megadataset,
        hours_lapso=12):
    if path.is_dir():
        a = start_date
        for i, file_path in enumerate(path.rglob("*.json")):
            name = file_path.stem
            try:
                counter = 0
                if not name == "metadata":
                    print("read json section ... i:>", i)

                    data = read_json(file_path)
                    data.sort(key=lambda x: x.timestamp)

                    last_date = start_date + timedelta(hours=hours_lapso)
                    counter = 0

                    start_index = 0
                    print("init", a, last_date)
                    dataset = []
                    for j, item in enumerate(data):
                        dt = datetime.fromisoformat(item.dt_gen)

                        if dt < start_date:
                            start_index = j

                        if dt > last_date:
                            tramo = slice(start_index, j)
                            megadataset[name].append(
                                tramo)
                            dataset = data[tramo]
                            start_index = j
                            last_date += timedelta(hours=hours_lapso)
                            # save dataset
                            dbyts = pickle.dumps(dataset)
                            path = Path(f"./datasets/{name}")
                            path.mkdir(parents=True, exist_ok=True)

                            path = path / f"{counter}.data"
                            path.write_bytes(dbyts)
                            counter += 1
                    del data
            except Exception as ex:
                print("Error", path, ex)
        return megadataset
    return []


if __name__ == "__main__":
    hours_lapso = 12
    base_path = Path("../DATA_BACKUP")
    path_base = Path("base_info.data")

    if not path_base.exists():
        start_date, last_date, last_index, megadataset = get_min_max_dt(
            base_path)

        databytes = pickle.dumps(
            (start_date, last_date, last_index, megadataset))
        path_base.write_bytes(databytes)

    else:
        databytes = path_base.read_bytes()
        start_date, last_date, last_index, megadataset = pickle.loads(
            databytes)

    print("dates", start_date, last_date)

    if (b := last_date - timedelta(days=7)) > start_date:
        start_date = b

    print("dates fix", start_date, last_date)

    read_json_section(base_path, start_date, last_date,
                      last_index, megadataset, hours_lapso)

    print(megadataset)
    databytes = pickle.dumps(megadataset)
    path_sections = Path("data_sections.data")
    path_sections.write_bytes(databytes)
