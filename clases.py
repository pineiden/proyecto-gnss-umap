import numpy as np
import pickle
import time
import ujson as json
from pathlib import Path
from datetime import datetime
from typing import List, Dict
from dacite import from_dict
from pydantic.dataclasses import dataclass
from pydantic import BaseModel, validator
from rich import print
from collections import OrderedDict


@dataclass
class Pair:
    value: float
    error: float


@dataclass
class Position:
    E: Pair
    N: Pair
    U: Pair


@dataclass
class TimeInfo:
    recv: str
    delta: float

    @property
    def dt_recv(self):
        return datetime.fromisoformat(self.value)


@dataclass
class DataItem:
    source: str
    station: str
    timestamp: int
    dt_gen: str
    data: Position
    time: TimeInfo

    @property
    def dt_dt_gen(self):
        return datetime.fromisoformat(self.value)

    @property
    def usar(self):
        return {
            "station": self.station,
            "timestamp": self.timestamp,
            "N": self.data.N.value,
            "E": self.data.E.value,
            "U": self.data.U.value
        }

    def pendiente(self, primer):
        delta = (self.timestamp-primer.timestamp) / 1000
        if delta > 0:
            return {
                "N": (self.data.N.value-primer.data.N.value)/(delta),
                "E": (self.data.E.value-primer.data.E.value)/(delta),
                "U": (self.data.U.value-primer.data.U.value)/(delta),
            }
        else:
            return {}


@dataclass
class GNSSDataset:
    items: Dict[str, List[DataItem]]

    def slice(self, t0: int, tf: int):
        megadataset_slice = {}
        for code, data_station in self.items.items():
            megadataset_slice[code] = [
                data for data in data_station if t0 <= data.timestamp
                < tf]

        clase = type(self)
        return clase(megadataset_slice)

    def __repr__(self):
        return f"GNSSDataset({len(self.items)})"

    @property
    def min_max(self):
        megadataset_slice = {}
        for name, data_station in self.items.items():
            lista = [
                data.timestamp for data in data_station]
            if lista:
                megadataset_slice[name] = (min(lista), max(lista))
        return megadataset_slice

    @property
    def abs_min_max(self):
        info = self.min_max.values()
        if info:
            return (max(info, key=lambda x: x[0])[0], min(info, key=lambda
                                                          x: x[1])[1])
        else:
            return (None, None)

    @property
    def abs_min_max(self):
        info = self.min_max.values()
        if info:
            return (min(info, key=lambda x: x[0])[0], max(info, key=lambda
                                                          x: x[1])[1])
        else:
            return (None, None)

    def bloque(self, ):
        (tmin, tmax) = self.abs_min_max
        return self.slice(tmin, tmax)

    def seleccion(self, lista: List[str]):
        clase = type(self)
        return clase({k: v for k, v in self.items.items() for k in lista})

    def pendiente(self, t0: int, tf: int):
        """
        data: un tramo pequeño de datos
        """
        seleccion = self.slice(t0, tf)
        pendientes = {}

        def metodo(data):
            primero = data[0]
            ultimo = data[-1]
            return ultimo.pendiente(primero)

        for code, data in seleccion.items.items():
            if data:
                pendientes[code] = metodo(data)
        return pendientes

    def pendientes(self, delta: int = 5):
        grupos = OrderedDict()

        abs_min, abs_max = self.abs_min_max
        # values from ms to seconds
        # drop to bottom
        abs_min = ((abs_min/1000)//delta)*delta
        # rise to top
        abs_max = ((abs_max/1000)//delta)*delta + delta

        def metodo(data):
            primero = data[0]
            ultimo = data[-1]
            return ultimo.pendiente(primero)

        for code, dataset in self.items.items():
            dataset = [d for d in dataset]
            tmin = (min(dataset, key=lambda x: x.timestamp).timestamp / 1000)
            tmax = (max(dataset, key=lambda x: x.timestamp).timestamp
                    / 1000)
            # reference time :: start
            tfirst = (tmin//delta) * delta
            tnext = tfirst + delta
            tramo_data = []
            # generate all empty blocks sequence
            # from abs_min t-> tnex
            # fill pre (padding)
            for this_time in range(int(abs_min), int(tfirst)-delta+1, delta):
                tramo_data.append({
                    "ts": this_time+delta,
                    "E": 0,  # float("inf"),
                    "N": 0,  # float("inf"),
                    "U": 0,  # float("inf")
                })
            tramo = []
            flag = True
            while dataset:
                data = dataset[0]
                if tfirst <= (data.timestamp/1000) < tnext:
                    if flag:
                        tramo.append(data)
                        flag = False
                    dataset.pop(0)

                elif (data.timestamp/1000) <= tnext+1:
                    # hay al menos dos datos para ese delta y se crea pendiente
                    tramo.append(data)
                    dataset.pop(0)
                    pdt = metodo(tramo)
                    pdt["ts"] = int(tnext//delta)*delta
                    tramo_data.append(pdt)
                    # breakpoint()
                    tramo = []
                    flag = True
                    tfirst = tnext
                    tnext = tfirst + delta
                else:
                    # No quitamos data de dataset. pero lo probamos en
                    # el siguiente tramo
                    tramo_data.append(
                        {
                            "ts": int(tnext),
                            "E": 0,  # float("inf"),
                            "N": 0,  # float("inf"),
                            "U": 0,  # float("inf")
                        })
                    # aumentamos el tramo de iteración
                    # e iniciamos nuevo tramo lista
                    tramo = []
                    flag = True
                    tfirst = int(tnext//delta)*delta
                    tnext = tfirst + delta
                # breakpoint()
            # fill post (padding)
            for this_time in range(int(tmax), int(abs_max)-delta+1, delta):
                tramo_data.append({
                    "ts": this_time+delta,
                    "E": 0,  # float("inf"),
                    "N": 0,  # float("inf"),
                    "U": 0,  # float("inf")
                })

            grupos[code] = tramo_data

        return grupos
